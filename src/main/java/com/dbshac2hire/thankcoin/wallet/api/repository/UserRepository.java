package com.dbshac2hire.thankcoin.wallet.api.repository;

import com.dbshac2hire.thankcoin.wallet.api.domain.User;
import java.util.Optional;
import org.springframework.data.repository.Repository;

/**
 *
 * @author isak.rabin
 */
public interface UserRepository extends Repository<User, Long> {

    /**
     * 
     * @param email
     * @return 
     */
    Optional<User> findByEmail(String email);
    
    /**
     *
     * @param email
     * @param password
     * @return
     */
    Optional<User> findByEmailAndPassword(String email, String password);

    /**
     *
     * @param id
     * @param email
     * @return
     */
    Optional<User> findByIdAndEmail(Long id, String email);

    /**
     *
     * @param id
     * @return
     */
    Optional<User> findById(Long id);

    /**
     * 
     * @param entity 
     * @return  
     */
    User save(User entity);

}
