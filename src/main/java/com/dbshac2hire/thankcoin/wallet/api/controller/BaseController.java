package com.dbshac2hire.thankcoin.wallet.api.controller;

import com.dbshac2hire.thankcoin.wallet.api.common.WalletSetting;
import com.dbshac2hire.thankcoin.wallet.api.domain.User;
import com.dbshac2hire.thankcoin.wallet.api.service.AccountService;
import com.dbshac2hire.thankcoin.wallet.api.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import javax.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 *
 * @author isak.rabin
 */
public abstract class BaseController {

    @Autowired
    protected WalletSetting setting;

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected UserService userService;

    public boolean isAuthenticate(String authorizationKey) {

        if (StringUtils.isEmpty(authorizationKey)) {
            return false;
        }

        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(setting.getAuthKey()))
                .parseClaimsJws(authorizationKey).getBody();
        String email = claims.getSubject();
        String id = claims.getId();

        User user;
        try {
            user = userService.findUserByIdAndEmail(Long.valueOf(id), email);
            if (user == null) {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }

        return true;
    }
}
