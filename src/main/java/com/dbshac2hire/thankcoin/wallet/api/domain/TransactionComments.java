package com.dbshac2hire.thankcoin.wallet.api.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author isak.rabin
 */
@Entity
public class TransactionComments implements Serializable {

    @Id
    private Long id;

    private String userId;

    private Long transactionId;

    private String comment;

    private Date lastUpdateDate;

    public TransactionComments() {
        lastUpdateDate = new Date();
    }

    public TransactionComments(String userId, Long transactionId, String comment) {
        this();
        this.userId = userId;
        this.transactionId = transactionId;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

}
