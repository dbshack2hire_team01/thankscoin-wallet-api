package com.dbshac2hire.thankcoin.wallet.api.controller;

import com.dbshac2hire.thankcoin.wallet.api.domain.User;
import com.dbshac2hire.thankcoin.wallet.api.service.dto.UserDto;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.AccountVo;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.LoginVo;
import com.dbshac2hire.thankcoin.wallet.api.service.vo.UserVo;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isak.rabin
 */
@RestController
public class UserController extends BaseController {

    /**
     *
     * @param user
     * @return
     */
    @RequestMapping(path = "/users/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<LoginVo> login(@RequestBody UserDto user) {
        LoginVo response = new LoginVo();

        if (StringUtils.isEmpty(user.getEmail()) || StringUtils.isEmpty(user.getPassword())) {
            response.setMessage("Invalid Parameter");
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            User entity = userService.findUserByEmailAndPassword(user.getEmail(), user.getPassword());
            if (entity != null) {
                String token = Jwts.builder()
                        .setId(String.valueOf(entity.getId()))
                        .setSubject(entity.getEmail())
                        .setIssuedAt(new Date())
                        .compressWith(CompressionCodecs.DEFLATE)
                        .signWith(SignatureAlgorithm.HS512, setting.getAuthKey())
                        .compact();
                response.setToken(token);
            } else {
                response.setMessage("Invalid email or password");
                return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            response.setMessage("Failed to authenticate");
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }

    /**
     *
     * @param user
     * @return
     */
    @RequestMapping(path = "/users/register", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<UserVo> register(@RequestBody UserDto user) {
        UserVo response = new UserVo();
        if (!StringUtils.isEmpty(user.getEmail()) && !StringUtils.isEmpty(user.getPassword())) {
            try {
                User entity = userService.register(user.getEmail(), user.getPassword());
                response.setEmail(entity.getEmail());
                response.setPassword(entity.getPassword());

                AccountVo account = accountService.init(entity.getId());
                response.setAccountNo(account.getNumber());
                response.setAccountBalance(account.getBalance());

            } catch (Exception ex) {
                response.setMessage("Failed to register user");
                return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
